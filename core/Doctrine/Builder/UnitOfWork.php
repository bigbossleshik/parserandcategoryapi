<?php

namespace Admin\core\Doctrine\Builder;
use Admin\src\Model\EntityInterface;
use Admin\core\Doctrine\Helper\DataBaze;
use Admin\Exception\MyException;
use ReflectionException;
class UnitOfWork
{
    const UPDATE = 'Update';
    const INSERT = 'Insert';
    const DELETE = 'Delete';


    private array $objects;
    private array $modification;


    public function __construct()
    {

        $this->objects = [];
        $this->modification = [];
    }


    public function batchadd(array $objs): self
    {
        foreach ($objs as $obj) {
            $obj1 = clone $obj;
            $this->objects[] = $obj1;
            $this->modification[] = $obj;
        }

        return $this;
    }

    public function persist(EntityInterface $entity): self
    {
        $this->modification[] = $entity;
        return $this;

    }

    public function remove(EntityInterface $entity): array
    {
        $key = array_search($entity, $this->modification);
        unset($this->modification[$key]);
        return $this->modification;

    }

    public function update(EntityInterface $entity)
    {
        foreach ($this->modification as $key => $item) {
            if ($item->getId() === $entity->getId()) {
                $this->modification[$key] = $entity;
            }
        }

        return $this->modification;
    }


    public function flush()
    {
        $this->beginTransaction();
        try {
            $masIdMod = $this->comparingArray($this->modification);
            $masIdObject = $this->comparingArray($this->objects);
            $resultArray = $this->collectingId($masIdMod, $masIdObject);
            $masObjectDelete = [];
            $masObjectUpdate = [];
            $masObjectInsert = [];

            foreach ($resultArray as $value) {
                $commonArray = explode('.', $value);

                switch ($commonArray[1]) {
                    case self::INSERT:
                    {
                        $masObjectInsert[] = $this->findObjectByIdInsertOrUpdateOrDelete($commonArray[0], $this->modification);

                        break;
                    }
                    case self::DELETE:
                    {
                        $masObjectDelete[] = $this->findObjectByIdInsertOrUpdateOrDelete($commonArray[0], $this->objects);
                        break;
                    }
                    case self::UPDATE:
                    {
                        $masObjectUpdate[] = $this->findObjectByIdInsertOrUpdateOrDelete($commonArray[0], $this->modification);

                        break;
                    }
                }
            }

            $this->process($masObjectInsert, $this->modification, self::INSERT);
            $this->process($masObjectDelete, $this->objects, self::DELETE);

            for ($i = 0; $i < count($masObjectUpdate); $i++) {
                $id = $this->getterId($masObjectUpdate[$i]);

                $reflection = new \ReflectionClass($masObjectUpdate[$i]);

                $reflectionProperty = $reflection->getProperties();

                $fieds = [];
                /** @var \ReflectionProperty $property */
                foreach ($reflectionProperty as $property) {
                    $fieds[] = $property->getName();
                }
                $getter = [];
                foreach ($fieds as $field) {
                    if ($field != 'id') {
                        $getter[] = 'get' . ucfirst($field);
                    }
                }

            }


            $this->process($masObjectUpdate, $this->objects, self::UPDATE);
        } catch (MyException $exception) {
            echo $exception->getMessage();
            $this->rollback();
        }
        $this->commit();
        return true;
    }

    public function findObjectByIdInsertOrUpdateOrDelete(int $id, array $mas)
    {

        foreach ($mas as $mod) {
            if ($id == $this->getterId($mod)) {
                return $mod;
            }
        }

    }

    public function process(array $array, array $modificationArray, string $modification)
    {
        $unit = new QueryBuilder();
        for ($i = 0; $i < count($array); $i++) {
            $id = $this->getterId($array[$i]);

            foreach ($modificationArray as $mod) {
                if ($id === $this->getterId($mod)) {

                    if (get_class($mod) === get_class($array[$i])) {
                        switch ($modification) {
                            case self::UPDATE:
                            {
                                $query = $unit->update($array[$i]);
                                break;
                            }
                            case self::DELETE:
                            {
                                $query = $unit->delete($array[$i]);
                                break;
                            }
                            case self::INSERT:
                            {
                                $query = $unit->insertInto($array[$i]);
                                break;
                            }
                        }

                        DataBaze::create()->quer($query);
                    }
                }
            }
        }
    }

    public function comparingArray(array $originalObject): array
    {

        $masIdMod = [];

        foreach ($originalObject as $mod) {
            if (!empty($this->getterId($mod))) {
                $masIdMod[] = $this->getterId($mod);
            } else {
                $masIdMod[] = $mod->getId();
            }
        }
        return $masIdMod;
    }

    public function collectingId(array $masIdMod, array $masIdObject): array
    {
        $resultArray = [];

        foreach ($masIdMod as $mod)
            if (in_array($mod, $masIdObject)) {
                $resultArray[] = $mod . '.' . self::UPDATE;

            } elseif (!in_array($mod, $masIdObject)) {
                $resultArray[] = $mod . '.' . self::INSERT;

            }
        foreach ($masIdObject as $item) {
            if (!in_array($item, $masIdMod)) {
                $resultArray[] = $item . '.' . self::DELETE;
            }
        }

        return $resultArray;
    }


    public function beginTransaction()
    {
        $query = 'START TRANSACTION;';
        return DataBaze::create()->quer($query);
    }

    public function rollback()
    {
        $query = 'ROLLBACK;';
        return DataBaze::create()->quer($query);
    }

    public function commit()
    {
        $query = 'COMMIT;';
        return DataBaze::create()->quer($query);
    }

    public function getterId(EntityInterface $entity)
    {
        $reflection = new \ReflectionClass($entity);
        $reflectionProperty = $reflection->getProperties();
        $fieldId = 'Admin\core\Doctrine\Builder\FieldId';

        $id = '';
        foreach ($reflectionProperty as $property) {
            $propertyAttributes = $property->getAttributes();
            foreach ($propertyAttributes as $attribute) {
                if ($fieldId == $attribute->getName()) {
                    $id = $property->getName();
                    $getterId = 'get' . ucfirst($id);
                    $result = $entity->$getterId();

                }

            }
        }
        return $result;
}
}