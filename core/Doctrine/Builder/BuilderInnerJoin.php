<?php

namespace Admin\core\Doctrine\Builder;
use Admin\core\Doctrine\Builder\InnerJoin;
class BuilderInnerJoin
{
    public ?string $innerJoin = null;
    public ?string $nameTableJoin = null;
    public ?string $nameColumnTable = null;
    public ?string $nameColumnJoin = null;

    public function init(): self
    {
        $this->innerJoin = null;
        $this->nameTableJoin = null;
        $this->nameColumnTable = null;
        $this->nameColumnJoin = null;
        return $this;
    }

    /**
     * @param string $innerJoin
     */
    public function setInnerJoin(string $innerJoin): self
    {
        $this->innerJoin = $innerJoin;
        return $this;
    }

    /**
     * @param string $nameTableJoin
     */
    public function setNameTableJoin(string $nameTableJoin): self
    {
        $this->nameTableJoin = $nameTableJoin;
        return $this;
    }

    /**
     * @param string $nameColumnTable
     */
    public function setNameColumnTable(string $nameColumnTable): self
    {
        $this->nameColumnTable = $nameColumnTable;
        return $this;
    }

    /**
     * @param string $nameColumnJoin
     */
    public function setNameColumnJoin(string $nameColumnJoin): self
    {
        $this->nameColumnJoin = $nameColumnJoin;
        return $this;
    }

    public function builder()
    {
        return new InnerJoin($this->innerJoin, $this->nameTableJoin, $this->nameColumnTable, $this->nameColumnJoin);
    }

}