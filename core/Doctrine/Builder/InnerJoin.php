<?php

namespace Admin\core\Doctrine\Builder;

class InnerJoin
{
    public string $innerJoin;
    public string  $nameTableJoin;
    public string  $nameColumnTable;
    public string  $nameColumnJoin;

    /**
     * @param string $innerJoin
     * @param string $nameTableJoin
     * @param string $nameColumnTable
     * @param string $nameColumnJoin
     */
    public function __construct(string $innerJoin, string $nameTableJoin, string $nameColumnTable, string $nameColumnJoin)
    {
        $this->innerJoin = $innerJoin;
        $this->nameTableJoin = $nameTableJoin;
        $this->nameColumnTable = $nameColumnTable;
        $this->nameColumnJoin = $nameColumnJoin;

    }

    public function create(
    )
    {
        if ($this->innerJoin != null) {
            $test = ' ' . 'INNER JOIN';
        }
        if ($this->nameTableJoin) {
            $test .= ' ' . $this->nameTableJoin . ' ' . 'ON';
        }
        if ($this->nameTableJoin && $this->nameColumnJoin) {
            $test .= ' ' . $this->nameColumnTable . ' ' . '=' . ' ' . $this->nameTableJoin . '.' . $this->nameColumnJoin;
        }
        return $test;
    }
}