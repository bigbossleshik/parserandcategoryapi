<?php

namespace Admin\core\Doctrine\Builder;

use Admin\core\Doctrine\Builder\UnitOfWork;
use Admin\Exception\MyException;
use Admin\src\Model\EntityInterface;
use Admin\core\Doctrine\Helper\DataBaze;
use Admin\src\Model\Hotel;
use Admin\core\Doctrine\Helper\HydratorObject;
use ReflectionClass;

class QueryBuilder
{
    public const COLUMN = '*';
    public const HYDRATION_ARRAY = 0;
    public const HYDRATION_OBJECT = 1;

    private array $limOfset ;
    private array $select;
    private array $joinMas = [];
    private ?string $nameTable = null;
    private array $where;
    private ?string $tableAlias = null;
    private string $query;
    private string $join;
    private string $className;
    private array $selectData;
    private UnitOfWork $ofWork;

    /**
     * @param UnitOfWork $ofWork
     */
    public function __construct()
    {
        $this->limOfset =[];
        $this->ofWork = new UnitOfWork();
    }


    public function select(string $select): self
    {
        if ($select === self::COLUMN) {
            $this->select[] = $select;

            if (strpos($select, ',')) {
                $this->select = explode(',', $select);
            }

        }
        return $this;
    }

    public function from(string $name, string $alias): self
    {

        if (!class_exists($name)) {
            throw new MyException('error');
        }
        /** @var EntityInterface $obj */
        $obj = new $name;
        $this->className = $name;
        $this->tableAlias = $alias;
        $this->nameTable = $obj->getTableName();

        return $this;
    }

    public function where(array $where): self
    {
        $this->where = $where;
        return $this;

    }

    public function limitAndOffset(int $lim, int $ofs): self
    {
        $this->limOfset =[
            'limit'=>$lim,
            'offset'=>$ofs
        ];

        return $this;
    }

    public function getQuery(): self
    {
        $this->query = 'SELECT' . ' ';
        $reflection = new \ReflectionClass($this->className);
        $fieldsObj = $reflection->getProperties();
        $fields = [];
        /** @var \ReflectionProperty $field */

        foreach ($fieldsObj as $field) {

            $fields[] = $field->getName();
        }
        $selectData = [];
        if ($this->select[0] === '*') {
            $selectData = $fields;
        } else {
            foreach ($this->select as $oneSelect) {
                $parts = explode('.', $oneSelect);
                if (count($parts) !== 2) {
                    throw new MyException('count != 2');
                }


                if (!in_array($parts[1], $fields)) {
                    throw new MyException('not in array');
                }

                $selectData[] = $oneSelect;
            }
        }
        $arrData = [];
        $this->selectData = $selectData;
        foreach ($this->selectData as $datum) {
            $arrData[] = $this->tableAlias . '.' . $datum;
        }
        $this->query .= implode(', ', $arrData);

        $this->query .= ' FROM ' . $this->nameTable . ' as ' . $this->tableAlias;
        if (count($this->limOfset)==2) {
            $this->query .= ' ' . 'LIMIT'.' ' . $this->limOfset['limit'] . ' '. 'OFFSET'. ' '. $this->limOfset['offset'] ;
        }
        if (!empty($this->join) && count($this->joinMas) != 0) {
            $this->query .= ' ' . 'JOIN' . ' ' . $this->join . ' ' . 'on' . ' ' . $this->joinMas[0][0] . ' ' . $this->joinMas[0][1] . ' ' . $this->join . '.' . $this->joinMas[0][2] . ' ';
        }
        if (!empty($this->where)) {
            $isFirst = true;
            $this->query .= ' WHERE ';
            foreach ($this->where as $where) {
                $whereString = $this->buildWhereParam($where);
                if ($isFirst === false) {
                    $this->query .= ' AND ';
                } else {
                    $isFirst = false;
                }

                $this->query .= ' ' . $whereString;
            }
        }

        return $this;
    }

    public function insertInto(EntityInterface $entity): string
    {
        $nameTable = 'Admin\src\Model\\';
        $lastName = $nameTable . $entity->getTableName();
        $nameTable2 = $entity->getTableName();
        $this->query = 'INSERT INTO' . ' ' . $nameTable2 . ' ';

        $reflection = new \ReflectionClass($lastName);
        $getMethod = $reflection->getMethods();
        $fieldsObj = $reflection->getProperties();
        $fields = [];
        /** @var \ReflectionProperty $field */
        foreach ($fieldsObj as $field) {
            $fields[] = $field->getName();
        }
        $methods = [];
        foreach ($getMethod as $method) {
            $methods[] = $method->getName();
        }
        array_shift($fields);


        $this->query .= '(' . implode(',', $fields) . ')' . ' ' . 'VALUES' . ' ' . '(';

        $arrayGettor = [];
        foreach ($fields as $field) {
            $getter = 'get' . ucfirst($field);
            if (method_exists($entity, $getter) && $getter != 'getId') {
                $arrayGettor[] = $getter;
            }
        }
        $obj[] = new $lastName;
        $arrayValue = [];
        foreach ($arrayGettor as $gets) {
            $obj = $entity->$gets();
            $arrayValue[] = $obj;
        }
        array_walk($arrayValue, fn(&$x) => $x = "'$x'");
        $lastResult = implode(',', $arrayValue);
        $this->query .= $lastResult . ')' . ';';

        return $this->query;
    }

    public function update(EntityInterface $entity): string
    {
        $nameTable = 'Admin\src\Model\\';
        $lastName = $nameTable . $entity->getTableName();
        $nameTable2 = $entity->getTableName();


        $this->query = 'UPDATE' . ' ' . $nameTable2 . ' ' . 'SET' . ' ';

        $reflection = new \ReflectionClass($lastName);
        $fieldsObj = $reflection->getProperties();
        $fields = [];
        foreach ($fieldsObj as $field) {
            $fields[] = $field->getName();
        }

        array_shift($fields);
        $arrayGetter = [];
        foreach ($fields as $value) {
            $getter = 'get' . ucfirst($value);
            if (method_exists($entity, $getter)) {
                $obj = $entity->$getter();
                $arrayGetter[] = $obj;

            }
        }
        $i = 0;

        foreach ($fields as $item) {


            if ($i != count($fields) - 1) {
                $this->query .= $item . ' ' . '=' . ' ' . "'$arrayGetter[$i]'" . ',';
                $i++;

            } else {

                $this->query .= $item . ' ' . '=' . ' ' . "'$arrayGetter[$i]'" . ' ';
                $i++;
            }
        }
        $this->query .= 'WHERE' . ' ' . 'id =' . ' ' . $entity->getId() . ';';

        return $this->query;
    }

    public function arrayJoin(array $mas): self
    {
        $this->joinMas = $mas;
        return $this;
    }

    public function joinF(string $name): self
    {
        if (!class_exists($name)) {
            throw new MyException('error');
        }
        $obj = new $name;
        $this->join = $obj->getTablename();
        return $this;
    }

    public function delete(EntityInterface $entity): string
    {
        $nameTable = $entity->getTableName();
        $where = $entity->getId();
        $this->query = 'DELETE FROM' . ' ' . $nameTable . ' ' . 'WHERE ' . 'id' . ' ' . '=' . ' ' . $where . ';';
        return $this->query;
    }

    public function getResult(int $hydration)
    {
        if (self::HYDRATION_ARRAY === $hydration) {

            return DataBaze::create()->quer($this->query);
        }
    }

    public function getResultSelect(int $hydration)
    {
        if (self::HYDRATION_ARRAY === $hydration) {
            return DataBaze::create()->allPrint($this->query);
        }

        if ($hydration === self::HYDRATION_OBJECT) {
            $dbresult = DataBaze::create()->allPrint($this->query);

            $arrayObj = HydratorObject::getFieldsMap($this->className, $this->selectData, $dbresult);
            $this->ofWork->batchAdd($arrayObj);
            return $arrayObj;
        }

        return [];
    }

    public function getSQL(string $s): string
    {
        $this->query = $s;
        return $s;
    }

    private function buildWhereParam(array $param): string
    {
        return ' ' . $this->tableAlias . '.' . $param[0] . ' ' . $param[1] . ' ' . $param[2];
    }
}