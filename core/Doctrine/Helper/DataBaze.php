<?php

namespace Admin\core\Doctrine\Helper;

use Admin\core\Doctrine\Builder\UnitOfWork;
use Admin\src\Model\EntityInterface;
use Admin\Exception\MyException;
class DataBaze
{
    private \mysqli $connect;
    private static ?DataBaze $baze = null;
    private const HOSTNAME = 'localhost:3306';
    private const USERNAME = 'root';
    private const PASS = 'root';
    private const DB = 'sve4i';
    private UnitOfWork $unitOfWork;

    private function __construct()
    {

        $this->unitOfWork = new UnitOfWork();
        $this->connect = \mysqli_connect(
            self::HOSTNAME,
            self::USERNAME,
            self::PASS,
            self::DB
        );

        if ($this->connect->connect_error) {
            throw new MyException('Error');
        }
    }

    public static function create()
    {
        if (self::$baze === null) {
            self::$baze = new DataBaze();
        }
        return self::$baze;
    }
    public function allPrint(string $selectInfo):array
    {
        return $this->connect->query($selectInfo)->fetch_all();
    }


    public function quer(string $a)
    {

        return $this->connect->query($a);
    }

    public function batchadd(array $mas)
    {
        return $this->unitOfWork->batchadd($mas);
    }
    public function persist(EntityInterface $entity)
    {

        return $this->unitOfWork->persist($entity);
    }

    public function flush()
    {

        return $this->unitOfWork->flush();
    }

    public function remove(EntityInterface $entity)
    {
        return $this->unitOfWork->remove($entity);
    }

    public function updateEntity(EntityInterface $entity)
    {
        return $this->unitOfWork->update($entity);
    }
}