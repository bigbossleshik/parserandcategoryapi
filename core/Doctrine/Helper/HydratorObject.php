<?php

namespace Admin\core\Doctrine\Helper;
use Admin\src\Model\EntityInterface;
use Admin\Exception\MyException;
class HydratorObject
{
    public static function getFieldsMap(string $className, array $fields, array $data)
    {
        $totalResult = [];

        foreach ($data as $oneData) {
            /** @var EntityInterface $obj */
            $obj = new $className();
            if (count($oneData) !== count($fields)) {
                throw new MyException('fields count not equals');
            }

            $result = array_combine($fields, $oneData);


            $fieldMap = explode('.', $fields[0]);
            $alias = $fieldMap[0] . '.';

            foreach ($fields as $field) {
                $shortField = str_replace($alias, '', $field);

                $shortField = self::toCamelCase($shortField);

                $setter = 'set' . ucfirst($shortField);

                $obj->$setter($result[$field]);
            }
            $totalResult[] = $obj;
        }

        return $totalResult;
    }

    private static function toCamelCase($string, $capitalizeFirstCharacter = false): string
    {

        $str = str_replace(' ', '', ucwords(str_replace('_', ' ', $string)));

        if (!$capitalizeFirstCharacter) {
            $str[0] = strtolower($str[0]);
        }

        return $str;
    }
}