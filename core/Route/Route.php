<?php

namespace Admin\core\Route;

use Admin\core\Events\EventsManager;
use Admin\core\Route\Events\PreRequestEvent;
use Admin\core\Route\Request;
use Admin\Exception\MyException;
use Admin\src\Controller\CategoryController;
use Admin\src\Controller\CandleController;


class Route
{
    public const GET = 'GET';
    public const POST = 'POST';
    public const PUT = 'PUT';
    public const DELETE = 'DELETE';

    public const MAP = [
        '/showCandle/{arg}'=>[CandleController::class,'showCandle', self::GET],
        '/addCandle'=> [CandleController::class,'addCandle', self::POST],
        '/removeCandle/{arg}'=>[CandleController::class,'removeCandle',self::GET],
        '/printCandle/{arg}'=>[CandleController::class,'printCandle',self::GET],
        '/updateCandle'=>[CandleController::class,'updateCandle',self::POST],
        '/newCatalog'=>[CategoryController::class,'newCatalog',self::POST],
        '/getCatalogById/{arg}'=>[CategoryController::class,'getCatalogById',self::GET],
        '/showCatalog/{arg}'=>[CategoryController::class,'showCatalog', self::GET],
        '/removeCatalog/{arg}'=>[CategoryController::class,'removeCatalog',self::GET],
        ];


    public function findPageByUriSymfony(Request $request)
    {

        $foundParams = [];
        $foundRoute = null;
        foreach (self::MAP as $route => $item) {

            if ($this->compareRouter2($request, $route, $item)) {
                $foundParams = $item;
                $foundRoute = $route;
                break;
            }

        }

        if (empty($foundParams)) {
            echo 'ROUTE NOT FOUND!!!';
            die;
        }
        $class = new $foundParams[0];
        $methods = $foundParams[1];

        try {


            $eventManager = EventsManager::getInstance();
            $eventManager->dispatch(new PreRequestEvent($foundParams[0] . ':' . $foundParams[1]));

            if (strpos($foundRoute, '{arg}')) {
                $param = $this->getParamBYMap($foundRoute, $request->getUrl());

                $lastResult = $class->$methods($param[0]);
            } else {
                $lastResult = $class->$methods();
            }

            return $lastResult;
        } catch (\Exception $e){
            echo $e->getMessage();
        }
    }

    public function compareRouter2(Request $request, string $route, array $item)
    {
        $method = $item[2] ?? self::GET;
        if ($request->getMethod() !== $method) {
            return false;
        }

        return $this->compareRouter($request->getUrl(), $route);

    }

    public function getParamBYMap(string $route, string $request): array
    {
        $requestParts = explode('/', $request);
        $routeParts = explode('/', $route);
        $result = [];
        foreach ($routeParts as $key => $param) {
            if ($param === '{arg}') {
                $result[] = $requestParts[$key];
            }
        }
        return $result;
    }

    public function compareRouter(string $uri, string $route): bool
    {
        $requestUri = explode('/', $uri);
        $requestRoute = explode('/', $route);

        if ($uri == $route) {
            return true;
        }

        if (count($requestRoute) != count($requestUri)) {
            return false;

        }
        foreach ($requestUri as $key => $str) {
            if ($str !== $requestRoute[$key] && $requestRoute[$key] !== '{arg}') {
                return false;
            }
        }
        return true;
    }
}