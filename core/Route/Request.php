<?php
namespace Admin\core\Route;

class Request
{

    public const GET = 'get';
    public const POST = 'post';
    public const PUT = 'put';
    public const DELETE = 'delete';


    private string $url;
    private string $method;
    private array $postData;
    private array $getData;
    private array $putData;

    public function  __construct() {
        $this->url = $_SERVER['REQUEST_URI'];
        $this->method = $_SERVER['REQUEST_METHOD'];
        $this->getData = $_GET;
        $this->postData = $_POST = json_decode(file_get_contents("php://input"), true) ?? [];
        //TODO: добавить сюда хедеры

    }

    public static function fromGlobals()
    {
        return new self();
    }

    public function get(string $key)
    {
        return $this->getData[$key] ?? null;
    }

    public function getAll()
    {
        return $this->getData ?? null;
    }

    public function post(string $key)
    {

        return $this->postData[$key] ?? null;

    }


    public function postAll()
    {
        return $this->postData ?? null;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }
}