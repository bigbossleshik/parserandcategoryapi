<?php

namespace Admin\core\Route\Events;


use Admin\core\Events\EventInterface;

class PreRequestEvent implements EventInterface
{

    private string $action;

    /**
     * @param string $action
     */
    public function __construct(string $action)
    {
        $this->action = $action;
    }

    public static function getName(): string
    {
        return self::class;
    }

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }
}