<?php

namespace Admin\core\Events;

interface EventInterface
{
    public static function getName(): string;
}