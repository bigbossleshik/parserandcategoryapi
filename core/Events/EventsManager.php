<?php

namespace Admin\core\Events;

class EventsManager
{
    private array $map = [];
    private static ?self $instance = null;


    private function __construct()
    {

    }

    public static function getInstance(): self
    {
        if(is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function subscribe(string $eventName, ListenerInterface $listener, int $priority = 0): void
    {
       
        $priority = $this->getFirstEmtyPriority($this->map[$eventName] ?? [], $priority);

        $this->map[$eventName][$priority] = $listener;

    }

    private function getFirstEmtyPriority(array $data, int $priority = 0): int
    {
        ksort($data);
        while (isset($data[$priority])) {
            $priority++;

        }
        return $priority;
    }

    public function dispatch(EventInterface $event): void
    {
        $listeners = $this->map[$event->getName()] ?? [];
        ksort($listeners);

        /** @var ListenerInterface $listener */
        foreach ($listeners as $listener) {
            $listener->process($event);
        }
    }

}