<?php

namespace Admin\core\Events;

interface ListenerInterface
{
    public function process(EventInterface $event);
}