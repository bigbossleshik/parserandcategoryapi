<?php

namespace Admin\src\Model;

interface EntityInterface
{
    public function getTableName();
}