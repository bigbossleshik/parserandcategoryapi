<?php

namespace Admin\src\Model;

use Admin\core\Doctrine\Builder\FieldId;

class Svechi implements EntityInterface
{
    #[FieldId]
    private int $id;
    private string $name;
    private string $linkImg;
    private string $price;
    private string $description;
    private int $categoryId;

    const NAME_CLASS ='svechi';

    public function getTableName()
    {
        return self::NAME_CLASS;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Svechi
     */
    public function setId(int $id): Svechi
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Svechi
     */
    public function setName(string $name): Svechi
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getLinkImg(): string
    {
        return $this->linkImg;
    }

    /**
     * @param string $linkImg
     * @return Svechi
     */
    public function setLinkImg(string $linkImg): Svechi
    {
        $this->linkImg = $linkImg;
        return $this;
    }

    /**
     * @return string
     */
    public function getPrice(): string
    {
        return $this->price;
    }

    /**
     * @param string $price
     * @return Svechi
     */
    public function setPrice(string $price): Svechi
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Svechi
     */
    public function setDescription(string $description): Svechi
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return $this->categoryId;
    }

    /**
     * @param int $categoryId
     * @return Svechi
     */
    public function setCategoryId(int $categoryId): Svechi
    {
        $this->categoryId = $categoryId;
        return $this;
    }

}