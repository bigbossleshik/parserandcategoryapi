<?php

namespace Admin\src\Model;

use Admin\core\Doctrine\Builder\FieldId;

class Category implements EntityInterface
{
    #[FieldId]
    private int $id;
    private string $name;
    private string $link;
    private int $parentId;
    const NAME ='category';
    public function getTableName()
    {
        return self::NAME;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Category
     */
    public function setId(int $id): Category
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Category
     */
    public function setName(string $name): Category
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @param string $link
     * @return Category
     */
    public function setLink(string $link): Category
    {
        $this->link = $link;
        return $this;
    }

    /**
     * @return int
     */
    public function getParentId(): int
    {
        return $this->parentId;
    }

    /**
     * @param int $parentId
     * @return Category
     */
    public function setParentId(int $parentId): Category
    {
        $this->parentId = $parentId;
        return $this;
    }

}