<?php

namespace Admin\src\Listener;

use Admin\core\Events\EventInterface;
use Admin\core\Events\ListenerInterface;
use Admin\core\Route\Events\PreRequestEvent;

class AuthListener implements ListenerInterface
{

    public function process(EventInterface $event)
    {
        if($event instanceof PreRequestEvent) {
            $headers = apache_request_headers();
            if(!isset($headers["X-Api-Key"]) || $headers["X-Api-Key"] != '12345') {
                echo 'Авторизируйся!!!!';
                DIE;
            }
        }

    }
}