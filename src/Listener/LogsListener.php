<?php

namespace Admin\src\Listener;

use Admin\core\Events\EventInterface;
use Admin\core\Events\ListenerInterface;
use Admin\core\Route\Events\PreRequestEvent;

class LogsListener implements ListenerInterface
{

    public function process(EventInterface $event)
    {
        if($event instanceof PreRequestEvent) {
            file_put_contents('requestLog.txt', $event->getAction() . PHP_EOL, FILE_APPEND);
        }

    }
}