<?php

namespace Admin\src\Service;

use Admin\core\Doctrine\Builder\QueryBuilder;
use Admin\core\Doctrine\Builder\UnitOfWork;
use Admin\core\Doctrine\Helper\DataBaze;
use Admin\Exception\MyException;
use Admin\src\Model\Category;
use Admin\src\Model\Svechi;

class SvechiService
{
    private QueryBuilder $queryBuilder;

    public function __construct()
    {
        $this->queryBuilder = new QueryBuilder();
    }

    public function printInfo(int $constanta): array
    {
        $resultPrint = $this->queryBuilder
            ->select('*')
            ->from(Svechi::class, 's')
            ->getQuery()
            ->getResultSelect($constanta);

        return $resultPrint;
    }

    public function addCandle(array $info): void
    {

        if (count($info) == 0) {
            throw new MyException('Json пуст');
        }
        if (!array_key_exists('name', $info) || !array_key_exists('linkImg', $info) || !array_key_exists('price', $info)
            || !array_key_exists('description', $info) || !array_key_exists('categoryId', $info)) {
            throw new MyException('Не все поля заполнены');
        } else {
            $infoCandle = $this->queryBuilder
                ->select('*')
                ->from(Category::class, 's')
                ->where([['id', '=', $info['categoryId']]])
                ->getQuery()
                ->getResultSelect(QueryBuilder::HYDRATION_OBJECT);
            if (count($infoCandle) <= 0) {
                throw new MyException('Ввели неверную категорию');
            }
            $svechi = new Svechi();
            $oneCandl = $svechi->setId(0)
                ->setName($info['name'])
                ->setLinkImg($info['linkImg'])
                ->setPrice($info['price'])
                ->setDescription($info['description'])
                ->setCategoryId($info['categoryId']);
            $this->printInfo(QueryBuilder::HYDRATION_OBJECT);
            DataBaze::create()->persist($oneCandl);
            DataBaze::create()->flush();
        }
    }

    public function showCandle(int $limit, int $page): array
    {
        if ($limit <= 0) {
            throw new MyException('Лимит введён неверно');
        } else {
            $ofset = $limit * ($page - 1);
        }
        if (empty($page)) {
            throw new MyException('Количетсов параметров на странице не было задано');

        }
        $findCount = $this->printInfo(QueryBuilder::HYDRATION_ARRAY);
        $count = count($findCount);
        if ($ofset >= $count) {
            throw new MyException('Задано слишком большое значение page');
        } else {
            $result = $this->queryBuilder
                ->select('*')
                ->from(Svechi::class, 's')
                ->limitAndOffset($limit, $ofset)
                ->getQuery()
                ->getResultSelect(QueryBuilder::HYDRATION_ARRAY);

            return $result;
        }
    }

    public function removeCandle(string $info): void
    {
        $infoCandle = $this->printInfo(QueryBuilder::HYDRATION_OBJECT);
        $arrayInfoCandle = $this->findCandle($info);
        if (empty($arrayInfoCandle[0])) {
            throw new MyException('Ничего не найдено по текущему id');
        } else {
            DataBaze::create()->batchadd($infoCandle);
            DataBaze::create()->remove($arrayInfoCandle[0]);
            DataBaze::create()->flush();
        }
    }

    public function findCandle($info): array
    {
        $arrInfo = explode('/', $info);
        $resultFindById = $this->queryBuilder
            ->select('*')
            ->from(Svechi::class, 's')
            ->where([['id', '=', $arrInfo[count($arrInfo) - 1]]])
            ->getQuery()
            ->getResultSelect(QueryBuilder::HYDRATION_OBJECT);
        if (count($resultFindById) <= 0) {
            throw new MyException('Товар по такому нмр не найден');
        } else {
            return $resultFindById;
        }
    }

    public function update(array $info): void
    {
        if (count($info) == 0) {
            throw new MyException('Json пуст');
        }
        if (!array_key_exists('name', $info) || !array_key_exists('linkImg', $info) || !array_key_exists('price', $info)
            || !array_key_exists('description', $info) || !array_key_exists('categoryId', $info)) {
            throw new MyException('Не все поля заполнены');
        } else {
            $svechi = new Svechi();
            $entity = $svechi
                ->setId($info['id'])
                ->setName($info['name'])
                ->setLinkImg($info['linkImg'])
                ->setPrice($info['price'])
                ->setDescription($info['description'])
                ->setCategoryId($info['categoryId']);
            DataBaze::create()->batchadd($this->printInfo(QueryBuilder::HYDRATION_OBJECT));
            $findCandleById = $this->queryBuilder
                ->select('*')
                ->from(Svechi::class, 's')
                ->where([['id', '=', $info['id']]])
                ->getQuery()
                ->getResultSelect(QueryBuilder::HYDRATION_ARRAY);
            if (count($findCandleById) <= 0) {
                throw new MyException('Элемента с таким Id не найдено');
            } else {
                DataBaze::create()->updateEntity($entity);
                DataBaze::create()->flush();
            }
        }
    }

}