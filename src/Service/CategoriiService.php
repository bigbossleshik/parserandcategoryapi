<?php

namespace Admin\src\Service;

use Admin\core\Doctrine\Builder\QueryBuilder;
use Admin\core\Doctrine\Helper\DataBaze;
use Admin\Exception\MyException;
use Admin\src\Model\Category;

class CategoriiService
{

    private QueryBuilder $queryBuilder;

    public function __construct()
    {
        $this->queryBuilder = new QueryBuilder();
    }

    public function getAll(int $con): array
    {
        $resultPrint = $this->queryBuilder
            ->select('*')
            ->from(Category::class, 'c')
            ->getQuery()
            ->getResultSelect($con);

        return $resultPrint;
    }

    public function getCatalogId(string $url): array
    {
        $findIdUrl = explode('/', $url);
        $findIdByDataBase = $this->queryBuilder
            ->select('*')
            ->from(Category::class, 'c')
            ->where([['id', '=', $findIdUrl[count($findIdUrl) - 1]]])
            ->getQuery()
            ->getResultSelect(QueryBuilder::HYDRATION_ARRAY);
        if (count($findIdByDataBase) <= 0) {
            throw new MyException('Элемент не был найден');
        } else {
            return $findIdByDataBase;
        }
    }

    public function addCategory(array $data): void
    {
        if (count($data) <= 0) {
            throw new MyException('Json пуст');
        }
        if (!array_key_exists('name', $data) || !array_key_exists('link', $data)
            || !array_key_exists('parentId', $data)) {
            throw new MyException('Не все поля заполнены');
        } else {
            $svechi = new Category();
            $oneCandl = $svechi->setId(0)
                ->setName($data['name'])
                ->setLink($data['link'])
                ->setParentId($data['parentId']);
            $this->getAll(QueryBuilder::HYDRATION_OBJECT);
            DataBaze::create()->persist($oneCandl);
            DataBaze::create()->flush();
        }

    }

    public function showCategory(int $page, int $limit): array
    {
        if ($limit <= 0) {
            throw new MyException('Лимит введён неверно');
        } else {
            $ofset = $limit * ($page - 1);
        }
        if (empty($page)) {
            throw new MyException('Количетсов параметров на странице не было задано');

        }
        $findCount = $this->getAll(QueryBuilder::HYDRATION_ARRAY);
        $count = count($findCount);
        if ($ofset >= $count) {
            throw new MyException('Задано слишком большое значение page');
        } else {
            $result = $this->queryBuilder
                ->select('*')
                ->from(Category::class, 'c')
                ->limitAndOffset($limit, $ofset)
                ->getQuery()
                ->getResultSelect(QueryBuilder::HYDRATION_ARRAY);

            return $result;
        }
    }

    public function removeCat(string $url): void
    {
        $infoCatalog = $this->getAll(QueryBuilder::HYDRATION_OBJECT);
        $cat = explode('/', $url);
        if (!is_numeric($cat[count($cat) - 1])) {
            throw new MyException('Неверно введены данные');
        } else {
            $resultFindById = $this->queryBuilder
                ->select('*')
                ->from(Category::class, 's')
                ->where([['id', '=', $cat[count($cat) - 1]]])
                ->getQuery()
                ->getResultSelect(QueryBuilder::HYDRATION_OBJECT);
            if (empty($resultFindById[0])) {
                throw new MyException('Товар по такому нмр не найден');
            } else {

                DataBaze::create()->batchadd($infoCatalog);
                DataBaze::create()->remove($resultFindById[0]);
                DataBaze::create()->flush();
            }
        }
    }
}