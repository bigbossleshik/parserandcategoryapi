<?php

namespace Admin\src\Controller;

use Admin\core\Route\Request;
use Admin\Exception\MyException;
use Admin\src\Helper\ResponsBuilder;
use Admin\src\Service\CategoriiService;

class CategoryController
{
    private CategoriiService $category;
    private ResponsBuilder $builder;

    public function __construct()
    {
        $this->builder = new ResponsBuilder();
        $this->category = new CategoriiService();
    }

    public function getRequest(): Request
    {
        return new Request();
    }

    public function getCatalogById(): void
    {
        try {
            $info = $this->getRequest()->getUrl();
            $res = $this->category->getCatalogId($info);
            $this->builder->successRespons($res);
        } catch (MyException $myException) {
            error_log($myException->getMessage() . PHP_EOL, 3, 'var/logs/error.log');
            throw new MyException($this->builder->fullAnswerErrorRespons($myException->getMessage()));
        }
    }

    public function newCatalog(): void
    {
        try {
            $info = $this->getRequest()->postAll();
            $this->category->addCategory($info);
            $this->builder->successfulAddition();
        } catch (MyException $myException) {
            error_log($myException->getMessage() . PHP_EOL, 3, 'var/logs/error.log');
            throw new MyException($this->builder->fullAnswerErrorRespons($myException->getMessage()));
        }
    }

    public function showCatalog():void
    {
        try {
            $limit = 4;
            $info = $this->getRequest()->getAll();
            if (!empty($info['page'])) {
                $page = $info['page'] ?? 1;
            } else {
                $page = 1;
            }
            $result = $this->category->showCategory($page, $limit);
            $this->builder->successRespons($result);

        } catch (MyException $myException) {
            error_log($myException->getMessage() . PHP_EOL, 3, 'var/logs/error.log');
            throw new MyException($this->builder->fullAnswerErrorRespons($myException->getMessage()));
        }
    }

    public function removeCatalog(): void
    {
        try {
            $info = $this->getRequest()->getUrl();
            $this->category->removeCat($info);
            $this->builder->successRemoveRespons();
        } catch (MyException $myException) {
            error_log($myException->getMessage() . PHP_EOL, 3, 'var/logs/error.log');
            throw new MyException($this->builder->fullAnswerErrorRespons($myException->getMessage()));
        }
    }
}