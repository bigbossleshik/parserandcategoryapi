<?php

namespace Admin\src\Controller;

use Admin\core\Route\Request;
use Admin\Exception\MyException;
use Admin\src\Helper\ResponsBuilder;
use Admin\src\Service\SvechiService;

class CandleController
{
    private SvechiService $service;
    private ResponsBuilder $responsBuilder;

    public function __construct()
    {
        $this->service = new SvechiService();
        $this->responsBuilder = new ResponsBuilder();
    }

    public function getRequest(): Request
    {
        return new Request();
    }

    public function showCandle(): void
    {
        try {
            $limit = 3;
            $info = $this->getRequest()->getAll();
            $page = !empty($info['page']) ? $info['page'] : 1;

            $result = $this->service->showCandle($limit, $page);
            $this->responsBuilder->successRespons($result);
        } catch (MyException $myException) {
            error_log($myException->getMessage() . PHP_EOL, 3, 'var/logs/error.log');
            throw new MyException($this->responsBuilder->fullAnswerErrorRespons($myException->getMessage()));
        }
    }

    public function addCandle(): void
    {
        try {
            $info = $this->getRequest()->postAll();
            $this->service->addCandle($info);
            $this->responsBuilder->successfulAddition();
        } catch (MyException $myException) {
            error_log($myException->getMessage() . PHP_EOL, 3, 'var/logs/error.log');
            throw new MyException($this->responsBuilder->fullAnswerErrorRespons($myException->getMessage()));
        }
    }

    public function removeCandle(): void
    {
        try {
            $info = $this->getRequest()->getUrl();
            $this->service->removeCandle($info);
            $this->responsBuilder->successRemoveRespons();
        } catch (MyException $myException) {
            error_log($myException->getMessage() . PHP_EOL, 3, 'var/logs/error.log');
            throw new MyException($this->responsBuilder->fullAnswerErrorRespons($myException->getMessage()));
        }
    }

    public function updateCandle():void
    {
        try {
            $info = $this->getRequest()->postAll();
            $this->service->update($info);
            $this->responsBuilder->successfulAddition();
        } catch (MyException $exception) {
            error_log($exception->getMessage() . PHP_EOL, 3, 'var/logs/error.log');
            throw new MyException(($this->responsBuilder->fullAnswerErrorRespons($exception->getMessage())));
        }
    }
}