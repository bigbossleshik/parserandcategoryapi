<?php

namespace Admin\src\Parser;

use Admin\core\Doctrine\Builder\QueryBuilder;
use Admin\src\Helper\CurlHelper;
use DiDom\Document;
use Krugozor\Database\Mysql;


class ParsingBuilder
{
    private array $findCategory=[];
    private array $findCategoryChildren=[];
    public function parserCategory($url):void
    {

        $curl = new CurlHelper();
        $result = $curl->printCurl($url);
        $phpQuert = \phpQuery::newDocument($result);
        $catalog = $phpQuert->find('.folders-shared')->children();
        foreach ($catalog as $key => $elCatalog) {
            $value = pq($elCatalog);
            $parent = $value->children('a');
            $this->findCategory[] = [
                'name' => $parent->text(),
                'link' => $parent->attr('href'),
                'parentId' => 0
            ];
            $categoryChildren = $value->find('li');
            foreach ($categoryChildren as $oneChildren) {
                $child = pq($oneChildren)->children('a');
                $this->findCategoryChildren[] =
                    [
                        'name' => $child->text(),
                        'link' => $child->attr('href'),
                        'parentId' => $key+1
                    ];
            }
        }
        $this->addDB($this->findCategory,'category');
        $this->addDB($this->findCategoryChildren,'category');
    }

    public function parserProduct($url):array
    {
        $curl = new CurlHelper();
        $resultArr = [];
        $lastArr = [];
        foreach ($this->findCategory as $key1=> $href) {
            $hrefRes = $curl->printCurl($url . $href['link']);
            $phpQ = \phpQuery::newDocument($hrefRes);
            $findList = $phpQ->find('.product-name a');
            foreach ($findList as $oneHref){
                $info = pq($oneHref)->attr('href');
                $lastArr[]=[
                    'href'=>$info,
                    'category'=>$key1+1
                ];
            }
        }

        foreach ($lastArr as $item){
            $lastRes = $curl->printCurl($url.$item['href']);
            $phpQuery = \phpQuery::newDocument($lastRes);

            $resultArr[]=[
                'name'=>$phpQuery->find('h1')->text(),
                'linkImg' =>"http://xn----ctbjmvlahmq4c.xn--90ais".$phpQuery->find(".site-container img")->attr('src'),
                'price' =>$phpQuery->find('.price-current strong')->text(),
                'description' => $phpQuery->find('.option-value')->text(),
                'categoryId'=> $item['category']
            ];

        }
        return $resultArr;
    }

    public function addDB(array $mas, string $nameCategoryDB):void
    {
        $db = Mysql::create('localhost', 'root', 'root')->setDatabaseName('sve4i');
        foreach ($mas as $last) {

            $db->query("INSERT INTO `$nameCategoryDB` SET ?As", $last);
    }

}
}


