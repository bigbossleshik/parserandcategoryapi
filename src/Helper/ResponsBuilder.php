<?php

namespace Admin\src\Helper;

class ResponsBuilder
{
    private const CONTENT_TYPE = 'Content-Type: application/json; charset=urf-8';

    public function build(array $content, int $resCode): void
    {
        http_response_code($resCode);
        header(self::CONTENT_TYPE);

        echo json_encode($content);
    }

    public function successfulAddition():void
    {
        $this->build(["message" => "The item was created successfully"],201);
    }

    public function successRespons(array $info):void
    {
        $this->build(['message'=>"Ok"],200);
        
        echo json_encode($info);
    }

    public function successRemoveRespons():void
    {
        $this->build(['message'=>"Ok"],200);
    }

    public function errorRespons():void
    {
        $this->build(['message'=>"The item does not exist"],404);
    }

    public function errorValidatorRespons(array $infoValidatorError):void
    {
        $this->build($infoValidatorError,400);
    }

    public function fullAnswerErrorRespons(string $error):void
    {
        $this->build(['message'=>"Ошибки проверки в вашем запросе",
                      'errors'=>$error ],400);
    }
}