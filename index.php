<?php
require_once 'vendor/autoload.php';

use Admin\core\Events\EventsManager;
use Admin\core\Route\Events\PreRequestEvent;
use Admin\core\Route\Request;
use Admin\core\Route\Route;
use Admin\src\Listener\AuthListener;
use Admin\src\Listener\LogsListener;
use Admin\src\Parser\ParsingBuilder;
//$a = new ParsingBuilder();
//$a->parserCategory('http://xn----ctbjmvlahmq4c.xn--90ais');

$eventsManager = EventsManager::getInstance();
$eventsManager->subscribe(PreRequestEvent::class, new AuthListener(), 4);
$eventsManager->subscribe(PreRequestEvent::class, new LogsListener(), 1);



$b = new Request();
$a = new Route();
$a->findPageByUriSymfony($b);


